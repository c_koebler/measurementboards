Version 4
SymbolType BLOCK
RECTANGLE Normal -64 -56 64 56
WINDOW 0 0 -56 Bottom 2
WINDOW 3 0 56 Top 2
SYMATTR Prefix X
SYMATTR Value TL072
SYMATTR ModelFile Z:\170302_ENCN2_Ansteuerkonzepte\4_HW_Entwicklung\Modularer_Inverter\Repositories\Modular_Inverter_VoltageMeas\Simulation\Bauteile\TL072.301
PIN -64 -16 LEFT 8
PINATTR PinName IN+
PINATTR SpiceOrder 1
PIN -64 16 LEFT 8
PINATTR PinName IN-
PINATTR SpiceOrder 2
PIN 64 -32 RIGHT 8
PINATTR PinName VCC
PINATTR SpiceOrder 3
PIN 64 32 RIGHT 8
PINATTR PinName VEE
PINATTR SpiceOrder 4
PIN 64 0 RIGHT 8
PINATTR PinName OUT
PINATTR SpiceOrder 5
